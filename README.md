**You can simply make a select dropdown as readonly(muted) as :**


```
#!javascript

$('.muted1').mutedSelect({ cursor : 'default', allowviewoptions : true });
```

* cursor (default value => "not-allowed"): you can set a cursor you want on the readonly select. Make sure you assign a cursor value which exists in w3c html standards

* allowviewoptions (default value => false) : You can either allow customer to view options while making it readonly, OR you can totally make is muted, i.e. not allowing viewer to check remaining options

**You can simply destroy the muted(readonly) functionality as :**


```
#!javascript

$('.muted1').mutedSelect('destroy');
```

When you have muted(readonly) a selectbox, you can still set its value programatically depending on your logic as :


```
#!javascript

$('.muted1').mutedSelect({'setValue': 'laravel'});
```

** Important : Make sure you include jquery library before muted-select js file!**

Built By : Mihir Bhende