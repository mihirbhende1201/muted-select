(function($) {
	$.fn.mutedSelect = function(options) {

 		this.each(function() {
	 		var selectBox = $(this);
	 		if(options == 'destroy'){
	 			var attr = selectBox.attr('refname');
					if (typeof attr !== typeof undefined && attr !== false) {
					  selectBox.removeAttr('refname');
					  selectBox.attr('name',attr);
					  $('input[name="'+ attr + '"]').remove();
					  selectBox.closest('.muted-container').find('.muted-overlay').remove();
					  selectBox.unwrap();
					}
					else{
						selectBox.unbind('change');
					}
					return true;
	 		}
	 		
	        var settings = $.extend({
	            cursor: "not-allowed",
	            setValue : "",
	            allowviewoptions : false
	        }, options );

	 		if(settings.setValue != ''){
	 			if($('option[value='+settings.setValue+']',selectBox).length > 0){
	 				selectBox.val(settings.setValue);
	 				var refname = selectBox.attr('refname');
	 				if (typeof refname !== typeof undefined && refname !== false) {
					  $('input[name="'+ refname + '"]').val(settings.setValue);
					}
	 			}
				return true;
	 		}

	 		if(settings.allowviewoptions == false){

		 		var selectName = selectBox.attr('name');
	 			selectBox.removeAttr('name');
	 			selectBox.attr('refname',selectName);
	 			var selectedOption = selectBox.val();
	 			var selectHeight = selectBox.outerHeight();
	 			var selectWidth = selectBox.outerWidth();
				$(selectBox).wrap( "<div class='muted-container' style='position:relative;display:inline-block;height:"+ selectHeight + ";width:"+ selectWidth + "'></div>" );
	 			selectBox.closest('.muted-container').append("<input name='"+ selectName +"' type='hidden' value='"+ selectedOption +"'>");
	 			selectBox.closest('.muted-container').append("<div class='muted-overlay' style='position:absolute;height:100%; width:100%; left:0;top:0;z-index:999;background:#ccc;opacity:0.5;cursor:"+settings.cursor +";'>");
	 				selectBox.on('change', function(e){
	 				var selectOption = $(this).val();
	 				$('input[name="'+ $(this).attr('refname') + '"]').val(selectOption);
	 			});
	 		}
	 		else if(settings.allowviewoptions == true){
	 			$("option:selected", selectBox).each(function(){
				    $(this).parent().data("default", this);
				});

				$(selectBox).bind('change',function(e) {
				    $($(this).data("default")).prop("selected", true);
				});
	 		}
	 		
	 	});	
        return true;
    };
}(jQuery));